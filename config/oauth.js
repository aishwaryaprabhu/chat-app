const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const keys = require(__dirname + '/keys.js');
const User = require("../models/user-model");	

passport.serializeUser((user, callback) =>{
	console.log("passport serializes");
	//its all about getting some info(user) and passing it to callback function for setting up cookies for browzer

	//when serialised userId will be assigned to cookie --step 1
	callback(null, user.googleId);
});

passport.deserializeUser((userId, callback) =>{
	//here when the cookie comes back, retieve id and get the user from database
	User.findOne({googleId : userId}, (err, user) =>{
		callback(null, user);
	});
});

passport.use(
	new GoogleStrategy({
		clientID: keys.google.clientID,
	    clientSecret: keys.google.clientSecret, 
		callbackURL: "/auth/login/redirect",		 //redirect after permission is granted
		accessType: 'offline'
	}, function(accessToken, refreshToken, profile, callback){
		console.log("passport callback");
		//callback(err, userobj);
		//first call extractGoogleProfile, next fire serializeuser method and send callback

		var newUser = {
			username : profile.name.givenName,
			fullname : profile.displayName,
			googleId : profile.id,
			profilePic : "",
			email : ""
		}
		if (profile.photos && profile.photos.length) {
			newUser.profilePic = profile.photos[0].value;
		}
	
		if (profile.emails && profile.emails.length) {
			newUser.email = profile.emails[0].value;	
		}
	
	
		User.findOne({googleId : profile.id}, (err, user) =>{
			if(user){
				callback(null, user);
			}else{
				var data = new User(newUser);
				data.save((err, user) => {
					if(err) {
						throw err;
					}
					callback(null, user);
				});
			}
		});
	}

));