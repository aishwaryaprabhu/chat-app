const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const userSchema = new Schema ({
	username: String,
	fullname: String,
	googleId: String,
	profilePic: String,
	email: String
});


const User = mongoose.model('user', userSchema);


module.exports = User;