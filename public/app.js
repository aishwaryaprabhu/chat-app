var chatApp = angular.module('chatApp', ['ui.router', 'ngCookies']);

var base_url = "http://localhost:3000/chatapp/";

chatApp.config(function($stateProvider, $urlRouterProvider){
	$urlRouterProvider.otherwise('/')

	var loginState = {
		name : 'login',						//state name
		url : '/',						//when state is active
		templateUrl : 'Auth/login.html'	//state view
	}	


	var homeState = {
		name : 'home',
		url : '/home',
		templateUrl : 'Home/home.html',
	}

	$stateProvider.state(loginState);
	$stateProvider.state(homeState);

});