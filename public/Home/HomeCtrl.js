function HomeCtrl($http, $state, $cookies){
    var home = this;
    var url = "home/home";
    var user = {};

    home.isTyping = "";
   // console.log($cookies.getAll());

    var socket = io.connect();

    home.chatArray = [{message : ""}];

    home.checkLoginStatus = function(){
        $http.post(url).then(function(response, status){
            if(response.status == 201){
                $state.go('login');
            }else{
                home.user = response.data;
                socket.emit('newUser', home.user.username);
            }
        })
    }

    home.setTyping = function(){
        console.log("typing");
        socket.emit('typing', home.user.username);
    }

    home.unsetTyping = function(){
        console.log("stopped");
        socket.emit('not typing');
    }

    socket.on('typing', function(username){
        home.isTyping = username + " typing";
        angular.element(document.getElementById("typing")).text(home.isTyping);
    });

    socket.on('not typing', function(){
        home.isTyping = "";
        angular.element(document.getElementById("typing")).text(home.isTyping);
    });

    socket.on('online', function(username){
        console.log(username);
        angular.element(document.getElementById("onlineuser")).append("<p>" + username + "</p>");
    });



    socket.on('userjoined', function(username){
        console.log(username);
        angular.element(document.getElementById("messages")).append("<li>" + username + " added!!</li>");
    });

    socket.on('newMessage', function(data){
        home.isTyping = "";
        angular.element(document.getElementById("typing")).text(home.isTyping);
        console.log(data);
        angular.element(document.getElementById("messages")).append("<li class='recieved'>" + data.username +" " + data.msg + "</li>");
        console.log(home.chatArray);
    });


    home.addMessage = function(){
        
        home.messageForm.$setPristine();
        home.messageForm.$setUntouched();
        home.messageForm.$setSubmitted();
        //home.chatArray.push({message : home.message});
        angular.element(document.getElementById("messages")).append("<li class='sent'>" + home.message + "</li>");
        //$("messages").append("<li>" + home.message + "</li>");
        var data = {
            username : home.user.username,
            msg : home.message
        }
        console.log(data);
        socket.emit('newMessage', data);
        home.message = "";
    }
}


chatApp.controller('HomeCtrl', HomeCtrl);