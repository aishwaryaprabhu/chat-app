const express = require("express")
const router = express.Router();
const passport = require('passport');

router.get(
	'/login', 
	passport.authenticate('google', {
		//google is the googlestratergy
		//scope is the user details 
		scope: ['email', 'profile']
	})
)


router.get(
	'/login/redirect', 
	passport.authenticate(
		'google', 
		{
			successRedirect :'/#!/home',
			faliureRedirect : '/#!/login'
		}
	), (req, res) =>{
	//passport.authenticate is a middleware to exchange code with google and calls callback in oauth.js
	//callback function after login with google
	// res.redirect('/home/home');
	res.status(200).send("Login Successfull");
})


router.get('/logout', (req, res) =>{
	req.logout();
	res.status(200).send("logging out...");
})

module.exports = router;