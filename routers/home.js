const express = require('express');
const router = express.Router();

const checkLoginStatus = (req, res, next)=>{
    //rconsole.log(req.session);
    if(req.user){
        next();
    }else{
        //res.redirect('/auth/login');
        res.status(201).send("user not logged in")
    }
}


router.post(
    '/home',
    checkLoginStatus,
    (req, res) => {
        res.status(200).send(req.user)
    }   
)

module.exports = router;