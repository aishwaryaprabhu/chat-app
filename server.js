const express = require("express");
const app = express();
const http = require("http").Server(app);
const io = require('socket.io')(http);

//const socket = require('socket.io);
//var io = socket(server);

const mongoose = require('mongoose');
const passport = require('passport');
const passportConfig = require(__dirname + "/config/oauth.js");
const keys = require(__dirname + '/config/keys.js');
const cookieSession = require('cookie-session');

app.use(        //encrypt the cookie  and send the cookie to browser--step 2
	cookieSession({
		maxAge : 60 * 60 * 1000,  //1 hour
		keys: [keys.session.cookieKey]  //encryption key
	})
)

//initalise passport(middleware)
app.use(passport.initialize());
//using session cookies 
app.use(passport.session());

//connect to mongodb
//localhost is server / which server
//if does not exit then the database will be created
//callback function fired when the connection is made.. 
mongoose.connect('mongodb://localhost/chatapp', () => {
	console.log("Database Connection successful");
})

app.use("/auth", require(__dirname + "/routers/login"));	
app.use("/home", require(__dirname + "/routers/home"));	

app.use('/', express.static(__dirname + "/public"));

// app.get('/', (req, res) => {
// 	res.status(200).send("Test");
// })	

const addedUser = false;

io.use((socket, next) => {
	if(socket.request.headers.cookie){ 
		addedUser = true;
		next();
	}else{
		return;
	}
});

io.on('connection', (socket) =>{
	console.log('User connected');

	socket.on('newUser', (username) =>{
		 if(addedUser){
			socket.username = username;
			io.sockets.emit('userjoined', socket.username);
			addedUser = false;
		//io.sockets.emit('userjoined', socket.username); //for all sockets connected	
		}

		io.sockets.emit('online', username);
	});

	socket.on('typing', function(username){
        socket.broadcast.emit('typing', username);
	});
	
	socket.on('not typing', function(){
        socket.broadcast.emit('not typing');
	});

	socket.on('newMessage', (data) => {
		var data = {
			username : data.username,
			msg : data.msg
		}
		console.log(data);
		socket.broadcast.emit('newMessage', data);
	});

	socket.on('disconnect', () => {
		console.log('user disconnected');
	});
});

const server  = http.listen(process.env.PORT || 2000, () => {
	const port = server.address().port;
	console.log("Listening to port " + port);
})